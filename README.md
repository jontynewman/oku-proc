# JontyNewman\Oku\Process

Functionality for generating cached output from an executed command.

This package is primarily intended for use with an instance of
`\JontyNewman\Oku\IO\Cache` from the
[`jontynewman\oku-io`](https://gitlab.com/jontynewman/oku-io)
package.

## Installation

```
composer require 'jontynewman/oku-proc ^1.0'
```

## Example

The following example assumes that
[`rst2html5`](http://docutils.sourceforge.net/docs/user/tools.html#rst2html5-py)
is available and that
[`jontynewman\oku-io`](https://gitlab.com/jontynewman/oku-io)
is installed.

```php
<?php

use GuzzleHttp\Psr7\Response;
use JontyNewman\Oku\IO\Cache;
use JontyNewman\Oku\IO\Repository;
use JontyNewman\Oku\Process;
use JontyNewman\Oku\RequestHandler;

require 'vendor/autoload.php';

$dir = '/path/to/io/directory';

// Convert reStructuredText input files to HTML output files.
$process = new Process('rst2html5');

// Cache HTML output in the specified directory.
$cache = new Cache($process, $dir, 'html');

// Persist reStructuredText input in the specified directory (for future edits).
$repository = new Repository($cache, $dir, 'rst');

// Use a simple 404 page as the default response.
$default = new Response(404, ['Content-Type' => 'text/plain'], 'Not Found');

// Set up the request handler.
$handler = new RequestHandler($repository, $default);

// Run the application.
$handler->run();

```

Refer to
[`jontynewman\oku-io`](https://gitlab.com/jontynewman/oku-io)
for a
[full example with an editor](https://gitlab.com/jontynewman/oku-io#example).
