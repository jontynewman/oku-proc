<?php

namespace JontyNewman\Oku;

use EmptyIterator;
use RuntimeException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * A cache callback that executes a command.
 */
class Process
{
	/**
	 * The default mode.
	 */
	const MODE = 0770;

	/**
	 * The current command.
	 *
	 * @var string
	 */
	private $cmd;

	/**
	 * The mode being used to create directories.
	 *
	 * @var int
	 */
	private $mode;

	/**
	 * Closes the given process and pipes.
	 *
	 * @param resource $process The process to close.
	 * @param iterable $pipes The pipes to close (if any).
	 * @throws RuntimeException The specified process and/or pipes could not be
	 * closed.
	 */
	public static function close($process, iterable $pipes = null): void
	{
		foreach ($pipes ?? new EmptyIterator() as $descriptor => $pipe) {

			$error = '';

			if (2 === $descriptor) {
				$error = stream_get_contents($pipe);
			}

			if (false === $error) {
				throw new RuntimeException('Failed to get pipe contents');
			}

			if (false === fclose($pipe)) {
				throw new RuntimeException('Failed to close pipe');
			}

			if ('' !== $error) {
				throw new RuntimeException($error);
			}
		}

		if (-1 === proc_close($process)) {
			throw new RuntimeException('Failed to close process');
		}
	}

	/**
	 * Constructs a cache callback that executes the given command.
	 *
	 * @param string $cmd The command to execute.
	 */
	public function __construct(string $cmd, int $mode = null)
	{
		$this->cmd = $cmd;
		$this->mode = $mode ?? self::MODE;
	}

	/**
	 * Converts the given input file to the given output file using the
	 * specified command.
	 *
	 * @param string $in The input file
	 * @param string $out The destination of the output file.
	 * @throws \RuntimeException The specified command failed to successfully
	 * execute.
	 */
	public function __invoke(string $in, string $out): void
	{
		$pipes = [];
		$descriptorspec = [
			0 => ['file', $in, 'rb'],
			1 => ['file', $out, 'wb'],
			2 => ['pipe', 'wb'],
		];

		(new Filesystem)->mkdir(dirname($out), $this->mode);

		$process = proc_open($this->cmd, $descriptorspec, $pipes);

		if (false === $process) {
			throw new RuntimeException('Unable to open process');
		}

		self::close($process, $pipes);
	}
}
