<?php

namespace JontyNewman\Oku\Tests;

use JontyNewman\Oku\Process;
use PHPUnit\Framework\TestCase;

class ProcessTest extends TestCase
{
	public function test(): void
	{
		$dir = sys_get_temp_dir();
		$in = tempnam($dir, 'in');
		$out = tempnam($dir, 'out');
		$php = '<?php echo "Hello, world!";';
		$proc = new Process('php');

		$this->assertSame(strlen($php), file_put_contents($in, $php));

		($proc)($in, $out);

		$this->assertSame('Hello, world!', file_get_contents($out));
	}
}